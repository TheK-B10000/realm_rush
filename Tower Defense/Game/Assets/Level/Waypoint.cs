﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Waypoint : MonoBehaviour {

    //[SerializeField] Color exploredColor; // TODO After Tutorial

    //public bool isExplored = false; // ok as is a data class

    public bool isExplored = false;
    public Waypoint exploredFrom;
    public bool isPlaceable = true;

    // Vector2Int - This structure is used in some places to represent 2D positions and 
    // vectors that don't require the precision of floating-point.
    Vector2Int gridPos;

    const int gridSize = 10;

    public int GetGridSize() //Get GridSize from CubeEditor
    {
        return gridSize;
    }

    void Start()
    {
        Physics.queriesHitTriggers = true;
    }

    // consider setting own color in Update()

    public Vector2Int GetGridPos() // Math to find coordinates
    {
        return new Vector2Int(
            Mathf.RoundToInt(transform.position.x / gridSize),  // ERROR 
            Mathf.RoundToInt(transform.position.z / gridSize)  // ALERT
            // Should be multipled by 10
        );
    }

    void OnMouseOver()
    {
        if (Input.GetMouseButtonDown(0)) // left click
        {
            if (isPlaceable)
            {
                FindObjectOfType<TowerFactory>().AddTower(this);
            }
            else
            {
                print("Can't place here!");
            }
        }
    }

}
