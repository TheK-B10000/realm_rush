﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode] // You can edit in Editor
[SelectionBase]     // ?? What does this do ??
[RequireComponent(typeof(Waypoint))] //Allows the use of Waypoint when needed
public class CubeEditor : MonoBehaviour
{


    //Vector3 gridPos; // The position of the cube in the grid??
    Waypoint waypoint; // creating gameobject of Waypoint

    private void Awake() //Start before Start
    {
        waypoint = GetComponent<Waypoint>(); // Get Waypoint
    }

    void Update()
    {
        SnapToGrid(); // Runs SnapToGrid Function
        UpdateLabel(); // Runs UpdateLabel Function

    }

    private void SnapToGrid() // The SnapToGrid is what allows the cubes to stay and move around in a grid
    {
        int gridSize = waypoint.GetGridSize();
        transform.position = new Vector3(
            waypoint.GetGridPos().x * gridSize,
            0f,
            waypoint.GetGridPos().y * gridSize
         );
        
 
    }

    private void UpdateLabel() //Updates Unity
    {
        TextMesh textMesh = GetComponentInChildren<TextMesh>();
        string labelText =
            waypoint.GetGridPos().x + 
            "," +
            waypoint.GetGridPos().y;
        textMesh.text = labelText;
        gameObject.name = labelText;
    }
}