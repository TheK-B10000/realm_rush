﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDamage : MonoBehaviour {

    [SerializeField] Collider collisionMesh;
    [SerializeField] int hitPoints = 10;
    [SerializeField] ParticleSystem hitParticePrefab;
    [SerializeField] ParticleSystem deathParticePrefab;

    // Use this for initialization
    void Start () {
		
	}
	private void OnParticleCollision(GameObject other)
    {
        //print("I'm hit!");
        ProcessHit();
        if (hitPoints <= 1)
        {
            KillEnemy();
        }
    }

	// Update is called once per frame
	void ProcessHit()
    {
        hitPoints = hitPoints - 1;
        hitParticePrefab.Play();
	}

    private void KillEnemy()
    {
        //hitPoints = 0;
        //important to instantiate before destroying this object
        var vfx = Instantiate(deathParticePrefab, transform.position, Quaternion.identity);
        vfx.Play();
        Destroy(gameObject);
    }
}
