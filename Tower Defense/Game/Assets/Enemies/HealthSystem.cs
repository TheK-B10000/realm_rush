﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthSystem : MonoBehaviour {

    [SerializeField] Collider collisionMesh;
    [SerializeField] GameObject Health;

    public const int maxHealth = 100;
    public int currentHealth = maxHealth;
   

	// Use this for initialization
	void Start () {
		
	}

    private void OnParticleCollision(GameObject other)
    {
        print("I'm Hit!");
    }

    public void TakeDamage(int amount)
    {
        currentHealth -= amount;
        if (currentHealth <= 0)
        {
            currentHealth = 0;
            Debug.Log("Dead!");
           // Destroy();
        }
    }

    
}
